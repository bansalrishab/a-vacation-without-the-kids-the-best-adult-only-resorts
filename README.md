**A Vacation Without the Kids: The Best Adult-Only Resorts**

Lots of times, couples who don’t have children or parents who want a vacation from children look for hotels that are adults only. Without having to worry about what words come out of their mouths at the dinner table or whether they’re going to step on someone’s sandcastle, many adults can have a more relaxing and a more enjoyable vacation knowing that children aren’t around. 

There are many adult-only resorts around the world. A quick search will find multiple options in the Caribbean, Mexico, and the Mediterranean. By researching a little deeper, there are more options available than you’d ever dreamed. Here are some of our favorites: 
Sonoma, California’s, Kenwood Inn & Spa features beautiful villas set in courtyards all with their own pool for guests over 18.

**[St gallen](http://www.oberwaid.ch/): One of the Best Wellness Hotel`s You Will Ever Visit**

This is a resort for the entire body, mind and soul. This is where you come to find luxury and relaxation. And this is where you will remember your stay for years to come. It’s not simply about amazing food, although we certainly offer that. Nor is it just about a luxurious spa treatment.

ARIA Resort & Casino in Las Vegas offers five-star luxury to those over 21 years of age.
Pennsylvania offers two choices in the Poconos Mountains for a refreshing adult-only holiday away from the beaches. With festivals all through the year and extreme sports, why not be adventurous? The two options are Cove Haven and Pocono Palace.

Australia has adult-only resorts, but Mai Tai Resort in Port Douglas is in the mountains with stunning sunset views of the rain forest. A well-kept secret that only accepts eight adult guests, you can be assured that peace and quiet is the order of the day.

Head to Thailand for a stay at Lavana Resort and Spa. Luxury, unique culture, and no children make for a peaceful holiday.

Fiji has many islands, but Viwa Island Resort is adult only. One of the beautiful Yasawa Islands, this is a place for romance, snorkeling, and diving. Environmentally-friendly and child-free makes for a tranquil holiday surrounded by coral reefs and brightly-colored fish. The island is covered in lush greenery and is small enough to walk round or across for a different view each day.

Sentido H10 White Suites, in Playa Blanca, is opening in November 2013 for luxury holidays for those 16 and over. This Canary Islands boutique hotel has a beauty center and a range of activities aimed at adults.

The island of Santorini, Greece, has several adult-only resorts and hotels with views over the dramatic Aegean Sea.

